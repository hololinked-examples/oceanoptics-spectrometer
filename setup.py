import setuptools

long_description="""
HTTP bindings for ocean optics spectrometer using seabreeze and hololinked.
Use it to control Ocean Optics spectrometers using HTTP or ZMQ in data-acquistion integrations, or 
writing web apps or IoT apps.
"""

setuptools.setup(
    name="oceanoptics-spectrometer",
    version="0.1.0",
    author="Vignesh Vaidyanathan",
    author_email="vignesh.vaidyanathan@hololinked.dev",
    description="HTTP bindings for ocean optics spectrometer using seabreeze",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/VigneshVSV/hololinked",
    packages=['oceanoptics_spectrometer'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],    
    install_requires=[
        "hololinked>=0.2.0",
        "seabreeze>=2.8.0",
    ],
    python_requires='>=3.7',
)
 