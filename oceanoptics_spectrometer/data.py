import numpy
from dataclasses import dataclass
from hololinked.server import JSONSerializer
from hololinked.server.td import JSONSchema


@dataclass 
class Intensity:
    value : numpy.ndarray
    timestamp : str  

    schema = {
        "type" : "object",
        "properties" : {
            "value" : {
                "type" : "array",
                "items" : {
                    "type" : "number"
                },
            },
            "timestamp" : {
                "type" : "string"
            }
        },
        "required" : ["value", "timestamp"],
        "additionalProperties" : False
    }

    @property
    def not_completely_black(self):
        if any(self.value[i] > 0 for i in range(len(self.value))):  
            return True 
        return False
    


JSONSerializer.register_type_replacement(numpy.ndarray, lambda obj : obj.tolist())
JSONSchema.register_type_replacement(Intensity, 'object', Intensity.schema)
