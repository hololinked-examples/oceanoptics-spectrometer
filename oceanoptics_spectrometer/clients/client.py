import os
import sys 
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import time
import asyncio
import logging
from device import OceanOpticsSpectrometer
from hololinked.client import ObjectProxy
from hololinked.server.utils import get_default_logger

logger = get_default_logger('client', logging.INFO)

# You can wrap this code in QT apps or scripting apps

spectrometer = ObjectProxy('spectrometer/ocean-optics/USB2000-plus', 
                        protocol='IPC', async_mixin=True, logger=logger) #type: OceanOpticsSpectrometer

if spectrometer.state == 'FAULT':
    spectrometer.reset_fault()
if spectrometer.state == 'MEASURING':
    spectrometer.stop_acquisition()
print("state : ", spectrometer.state)

# set and get properties
spectrometer.integration_time = 500
print("integration time : ", spectrometer.integration_time)
spectrometer.integration_time = 300
print("integration time : ", spectrometer.integration_time)
spectrometer.background_correction = None


# set and get multiple properties
print(spectrometer.get_properties(names=["integration_time", "trigger_mode"]))
spectrometer.set_properties(
    integration_time=100, 
    nonlinearity_correction=False
)
print(spectrometer.get_properties(names=["state", "nonlinearity_correction", 
                                    "integration_time", "trigger_mode"]))

# schedule events
def callback(data):
    print("got data - timestamp", data["timestamp"], ", length - ", len(data), ", 0th and last value", 
          data["value"][0], data["value"][-1])


spectrometer.intensity_measurement_event.subscribe(callback)
spectrometer.start_acquisition()
time.sleep(10)
print("stopping acquisition")
spectrometer.stop_acquisition()


# async examples 
async def async_examples():
    global spectrometer

    # get property
    print(await spectrometer.async_get_property("integration_time"))
    # set property
    await spectrometer.async_set_property("integration_time", 500)
    print(await spectrometer.async_get_property("integration_time"))
    # set properties
    await spectrometer.async_set_properties(integration_time=200, background_correction=None)
    # get properties
    print(await spectrometer.async_get_properties(names=["state", "nonlinearity_correction", 
                                    "integration_time", "trigger_mode", "background_correction"]))
    # invoke methods
    await spectrometer.async_invoke("start_acquisition")
    await asyncio.sleep(5)
    await spectrometer.async_invoke("stop_acquisition")


loop = asyncio.get_event_loop()
loop.run_until_complete(async_examples())

print(spectrometer.get_properties(names=["state", "nonlinearity_correction", 
                                    "integration_time", "trigger_mode"]))

# oneway and noblock examples 
msg_id = spectrometer.get_property('integration_time', noblock=True)
spectrometer.invoke("start_acquisition", oneway=True)
time.sleep(5)
spectrometer.invoke("stop_acquisition", oneway=True)
print("integration time oneway read", spectrometer.read_reply(msg_id, timeout=500))

spectrometer.intensity_measurement_event.unsubscribe()
