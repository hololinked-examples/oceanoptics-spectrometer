import datetime
import threading
import typing
import numpy
import time
from enum import StrEnum
from seabreeze.spectrometers import Spectrometer, SeaBreezeError 

from hololinked.server import Thing, action, Event
from hololinked.server.properties import (String, Integer, Number, List, Boolean,
                                    Selector, ClassSelector, TypedList)
from hololinked.server import StateMachine

from data import Intensity
from schema import *


class States(StrEnum):
    DISCONNECTED = "DISCONNECTED"
    ON = "ON"
    FAULT = "FAULT"
    MEASURING = "MEASURING"
    ALARM = "ALARM"
    SIMULATION = "SIMULATION"


class OceanOpticsSpectrometer(Thing):
    """
    OceanOptics spectrometers using seabreeze library. Device is identified with specifying serial number. 
    For supported spectrometers visit : <br />
    <a href="https://python-seabreeze.readthedocs.io/en/latest/">Seabreeze Pypi</a> <br />
    <a href="https://github.com/ap--/python-seabreeze/tree/main">Seabreeze GitHub</a>
    """

    states = States

    status = String(readonly=True, fget=lambda self: self._status,
                    doc="descriptive status of current operation") # type: str

    serial_number = String(default=None, allow_None=True, 
                            doc="serial number of the spectrometer to connect/or connected") # type: str

    last_intensity = ClassSelector(default=None, allow_None=True, class_=Intensity, 
                            doc="last measurement intensity (in arbitrary units)") # type: Intensity
    
    intensity_measurement_event = Event(friendly_name='intensity-measurement-event', schema=Intensity.schema,
            doc="event generated on measurement of intensity, max 30 per second even if measurement is faster.")
            
    reference_intensity = ClassSelector(default=None, allow_None=True, class_=Intensity,
            doc="reference intensity to overlap manually in the background in a GUI") # type: Intensity
    
  
    def __init__(self, instance_name : str, serial_number : typing.Optional[str] = None, **kwargs) -> None:
        super().__init__(instance_name=instance_name, serial_number=serial_number, **kwargs)
        if serial_number is not None:
            self.connect()
        self._acquisition_thread = None 
        self._running = False
       
    def set_status(self, *args) -> None:
        if len(args) == 1:
            self._status = args[0]
        else:
            self._status = ' '.join(args)
            
    @action(input_schema=connect_args)
    def connect(self, serial_number : str = None, trigger_mode : int = None, integration_time : float = None,
                    use_first_available : bool = True) -> None:
        """connect to spectrometer with serial number & apply trigger mode and integration time"""
        if serial_number is not None:
            self.serial_number = serial_number
        if serial_number is None and self.serial_number is None:
            self.device = Spectrometer.from_first_available()
        elif use_first_available:
            self.device = Spectrometer.from_serial_number(self.serial_number)
        else:
            raise ValueError("serial number not provided")
        self.serial_number = self.device.serial_number
        self.state_machine.current_state = self.states.ON
        self._wavelengths = self.device.wavelengths().tolist()
        self._model = self.device.model
        self._pixel_count = self.device.pixels
        self._max_intensity = self.device.max_intensity
        if trigger_mode is not None:
            self.trigger_mode = trigger_mode
        else:
            self.trigger_mode = self.trigger_mode
            # Will set existing or default value of property
        if integration_time is not None:
            self.integration_time = integration_time
        else:
            self.integration_time = self.integration_time
            # Will set existing or default value of property
        self.logger.debug(f"opened device with serial number {self.serial_number} with model {self.model}")
        self.set_status("ready to start acquisition")

    model = String(default=None, allow_None=True, readonly=True,
                doc="model of the connected spectrometer",
                fget=lambda self: self._model if self.state_machine.current_state != self.states.DISCONNECTED else None
                ) # type: str
    
    wavelengths = List(default=None, allow_None=True, item_type=(float, int), readonly=True,
                    doc="wavelength bins of measurement",
                    fget=lambda self: self._wavelengths if self.state_machine.current_state != self.states.DISCONNECTED else None,
                ) # type: typing.List[typing.Union[float, int]]

    pixel_count = Integer(default=None, allow_None=True, readonly=True,
                doc="number of points in wavelength",
                fget=lambda self: self._pixel_count if self.state_machine.current_state != self.states.DISCONNECTED else None
                ) # type: int
    
    max_intensity = Number(readonly=True,
                    doc="""the maximum intensity that can be returned by the spectrometer in (a.u.). 
                        It's possible that the spectrometer saturates already at lower values.""",
                    fget=lambda self: self._max_intensity if self.state_machine.current_state != self.states.DISCONNECTED else None
                    ) # type: float
      
    @action()
    def disconnect(self):
        """disconnect from the spectrometer"""
        self.device.close()
        self.state_machine.current_state = self.states.DISCONNECTED
        self.set_status("disconnected")

    trigger_mode = Selector(objects=[0, 1, 2, 3, 4], default=0, observable=True,
                        doc="""0 = normal/free running, 1 = Software trigger, 2 = Ext. Trigger Level,
                         3 = Ext. Trigger Synchro/ Shutter mode, 4 = Ext. Trigger Edge""") # type: int
    
    @trigger_mode.setter 
    def apply_trigger_mode(self, value : int):
        self.device.trigger_mode(value)
        self._trigger_mode = value 
        
    @trigger_mode.getter 
    def get_trigger_mode(self):
        try:
            return self._trigger_mode
        except:
            return self.properties["trigger_mode"].default 
        

    integration_time = Number(default=1000, bounds=(0.001, None), crop_to_bounds=True, observable=True,
                            doc="integration time of measurement in milliseconds") # type: float
    
    @integration_time.setter 
    def apply_integration_time(self, value : float):
        self.device.integration_time_micros(int(value*1000))
        self._integration_time = int(value) 
      
    @integration_time.getter 
    def get_integration_time(self) -> float:
        try:
            return self._integration_time
        except:
            return self.properties["integration_time"].default 
  
    @action()
    def edit_intregation_time(self, **kwargs) -> None:
        if kwargs.get('bounds', None):
            value = kwargs.get('bounds')
            if not isinstance(value, list) and len(value) == 2:
                raise TypeError("Specify integration time bounds as a list of two values [lower bound, higher bound]")
            if not isinstance(value[0], (type(None), (int, float))):
                raise TypeError("lower bound of integration type not a number or None")
            if not isinstance(value[1], (type(None), (int, float))):
                raise TypeError("higher bound of integration type not a number or None")                                          
            self.properties["integration_time"].bounds = value 
        raise NotImplementedError("only bounds can be edited for integration time property.")
    
    
    background_correction = Selector(objects=['AUTO', 'CUSTOM', None], default=None, allow_None=True, 
                        doc="set True for Seabreeze internal black level correction") # type: typing.Optional[str]
    
    custom_background_intensity = TypedList(item_type=(float, int), 
                        doc="user supplied intensity for background correction") # type: typing.List[typing.Union[float, int]]
    
    nonlinearity_correction = Boolean(default=False, 
                        doc="automatic correction of non linearity in detector CCD") # type: bool

    @action()
    def start_acquisition(self, max_count : typing.Optional[int] = None) -> None:
        """start continuous acquisition in a separate thread, data is pushed as events, subscribe to intensity-measurement-event"""
        self.stop_acquisition() # Just a shield 
        self._acquisition_thread = threading.Thread(target=self.measure, args=(max_count,)) 
        self._acquisition_thread.start()
       

    @action()
    def stop_acquisition(self) -> None:
        """stop continuous acquisition"""
        if self._acquisition_thread is not None:
            self.logger.debug(f"stopping acquisition thread with thread-ID {self._acquisition_thread.ident}")
            self._running = False # break infinite loop
            # Reduce the measurement that will proceed in new trigger mode to 1ms
            self.device.integration_time_micros(1000)       
            # Change Trigger Mode if anything else other than 0, which will cause for the measurement loop to block permanently 
            self.device.trigger_mode(0)                    
            self._acquisition_thread.join()
            self._acquisition_thread = None 
            # re-apply old values
            self.trigger_mode = self.trigger_mode
            self.integration_time = self.integration_time 
        

    def measure(self, max_count = None):
        try:
            self._running = True
            self.state_machine.current_state = self.states.MEASURING
            self.logger.info(f'starting continuous acquisition loop with trigger mode {self.trigger_mode} & integration time {self.integration_time} in thread with ID {threading.get_ident()}')
            loop = 0
            while self._running:
                if max_count is not None and loop > max_count:
                    break 
                self.set_status("measuring")
                loop += 1
                try:
                    # Following is a blocking command - self.spec.intensities
                    self.logger.debug(f'starting measurement count {loop}')
                    
                    _current_intensity = self.device.intensities(
                                                        correct_dark_counts=True if self.background_correction == 'AUTO' else False,
                                                        correct_nonlinearity=self.nonlinearity_correction
                                                    )
                                 
                    if self.background_correction == 'CUSTOM':
                        if self.custom_background_intensity is None:
                            self.logger.warn('no background correction possible')
                            self.state_machine.set_state(self.states.ALARM)
                        else:
                            _current_intensity = _current_intensity - self.custom_background_intensity

                    curtime = datetime.datetime.now()
                    timestamp = curtime.strftime('%d.%m.%Y %H:%M:%S.') + '{:03d}'.format(int(curtime.microsecond /1000))
                    self.logger.debug(f'measurement taken at {timestamp} - measurement count {loop}')

                    if self._running:
                        # To stop the acquisition in hardware trigger mode, we set running to False in stop_acquisition() 
                        # and then change the trigger mode for self.spec.intensities to unblock. This exits this 
                        # infintie loop. Therefore, to know, whether self.spec.intensities finished, whether due to trigger 
                        # mode or due to actual completion of measurement, we check again if self._running is True. 
                        self.last_intensity = Intensity(
                            value=_current_intensity, 
                            timestamp=timestamp
                        )
                        if self.last_intensity.not_completely_black:   
                            self.intensity_measurement_event.push(self.last_intensity)
                            self.state_machine.current_state = self.states.MEASURING
                        else:
                            self.logger.warn('trigger delayed or no trigger or erroneous data - completely black')
                            self.state_machine.current_state = self.states.ALARM
                except SeaBreezeError as ex:
                    if not self._running and 'Data transfer error' in str(ex):
                        pass
                    else:
                        self.set_status(f'error during acquisition - {str(ex)}')
                        raise ex from None
               
            if self.state_machine.current_state not in [self.states.FAULT, self.states.ALARM]:        
                self.state_machine.current_state = self.states.ON
                self.set_status("ready to start acquisition") # set status only if there is no cause for error
            self.logger.info("ending continuous acquisition") 
            self._running = False 
        except Exception as ex:
            self.logger.error(f"error during acquisition - {str(ex)}, {type(ex)}")
            self.set_status(f'error during acquisition - {str(ex)}, {type(ex)}')
            self.state_machine.current_state = self.states.FAULT
       

    @action()
    def start_acquisition_single(self):
        """Carry out a single measurement, event will be pushed at the end"""
        self.stop_acquisition() # Just a shield 
        self._acquisition_thread = threading.Thread(target=self.measure, args=(1,)) 
        self._acquisition_thread.start()
        self.logger.info("data event will be pushed once acquisition is complete.")

    @action()
    def reset_fault(self):
        """Reset state machine to ON state when in FAULT state"""
        self.state_machine.set_state(self.states.ON)

    def simulate_measurement(self, max_count):
        self._running = True
        self.state_machine.current_state = self.states.SIMULATION
        self.set_status("simulating")
        self.logger.info(f'starting simulating loop integration time {self.integration_time} in thread with ID {threading.get_ident()}')
        loop = 0
        while self._running:
            if max_count is not None and loop > max_count:
                break 
            loop += 1
            try:
                # Following is a blocking command - self.spec.intensities
                self.logger.debug(f'starting measurement count {loop}')
                _current_intensity = numpy.random.randint(0, self.max_intensity or 16384, self.pixel_count or 1024)
                time.sleep(self.integration_time/1000 if self.integration_time is not None else 1000)

                curtime = datetime.datetime.now()
                timestamp = curtime.strftime('%d.%m.%Y %H:%M:%S.') + '{:03d}'.format(int(curtime.microsecond /1000))
                self.logger.debug(f'measurement taken at {timestamp} - measurement count {loop}')

                self.intensity_measurement_event.push({
                    "value": _current_intensity.tolist(),
                    "timestamp": timestamp   
                })
                self.state_machine.current_state = self.states.SIMULATION
            except Exception as ex:
                self.logger.error(f"error during acquisition - {str(ex)}, {type(ex)}")
                self.set_status(f'error during acquisition - {str(ex)}, {type(ex)}')
                self.state_machine.current_state = self.states.ALARM 
        self.state_machine.current_state = self.states.ON
        self.set_status("")

    @action()
    def simulate(self, max_count : typing.Optional[int] = None):
        self._acquisition_thread = threading.Thread(target=self.simulate_measurement, args=(max_count,))
        self._acquisition_thread.start()
    
    @action()
    def stop_simulation(self):
        self._running = False

    state_machine = StateMachine(
        states=states,
        initial_state=states.DISCONNECTED,
        push_state_change_event=True,
        DISCONNECTED=[connect, serial_number, simulate],
        ON=[start_acquisition, start_acquisition_single, disconnect, simulate,
            integration_time, trigger_mode, background_correction, nonlinearity_correction],
        MEASURING=[stop_acquisition],
        FAULT=[stop_acquisition, reset_fault],
        SIMULATION=[stop_simulation]
    )

    logger_remote_access = True