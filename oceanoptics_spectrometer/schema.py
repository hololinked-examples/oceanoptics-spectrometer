import msgspec

connect_args = {
    "type": "object",
    "properties": {
        "serial_number": {"type": "string"},
        "trigger_mode": {"type": "integer"},
        "integration_time": {"type": "number"},
        "use_first_available": {"type": "boolean"}
    },
    "additionalProperties": False
}


# class connect_args_struct(msgspec.Struct):
#     serial_number: str
#     trigger_mode: int
#     integration_time: float

#     @classmethod
#     def from_dict(cls, data: dict) -> "connect_args_struct":
#         return cls(
#             serial_number=data.get("serial_number"),
#             trigger_mode=data.get("trigger_mode"),
#             integration_time=data.get("integration_time")
#         )

#     def to_dict(self) -> dict:
#         return {
#             "serial_number": self.serial_number,
#             "trigger_mode": self.trigger_mode,
#             "integration_time": self.integration_time
#         }
