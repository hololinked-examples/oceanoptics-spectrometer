import ssl, os, logging
from multiprocessing import Process
from hololinked.server import HTTPServer
from device import OceanOpticsSpectrometer


def start_https_server():
    # You need to create a certificate on your own 
    ssl_context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS_SERVER)
    ssl_context.load_cert_chain(f'assets{os.sep}security{os.sep}certificate.pem',
                        keyfile = f'assets{os.sep}security{os.sep}key.pem')
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_3

    H = HTTPServer(['spectrometer'], port=9003, ssl_context=ssl_context, 
                      log_level=logging.DEBUG)  
    H.listen()


def start_http_server():
    H = HTTPServer(['spectrometer'], port=9003,
                      log_level=logging.DEBUG)  
    H.listen()


def multiprocess_example():
    P = Process(target=start_https_server) 
    # change to start_http_server if no certificate available
    P.start()

    O = OceanOpticsSpectrometer(
        instance_name='spectrometer',
        # serial_number='USB2+H15897',
        log_level=logging.DEBUG
    )
    O.run()


def multithreaded_example():
    ssl_context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS_SERVER)
    ssl_context.load_cert_chain(f'assets{os.sep}security{os.sep}certificate.pem',
                        keyfile = f'assets{os.sep}security{os.sep}key.pem')
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_3


    O = OceanOpticsSpectrometer(
        instance_name='spectrometer',
        # serial_number='USB2+H15897',
        log_level=logging.DEBUG
    )
    O.run_with_http_server(
        port=9003, ssl_context=ssl_context, 
        log_level=logging.DEBUG
    )


if __name__ == "__main__":
    # multithreaded_example()
    multiprocess_example()