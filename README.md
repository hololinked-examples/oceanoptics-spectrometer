# HTTP for Oceanoptics Spectrometer

Seabreeze package extension to support HTTP bindings with `hololinked`. Currently no security except encryption (https).

ZMQ client possible over TCP & interprocess-communication (suitable for Qt apps/scripts).
Check example `oceanoptics_spectrometer/clients/holoviz_panel.ipynb`.

pip installable. Clone or download, then `pip install .` or `pip install -e .` for develop mode. 

For a beginner's example of server, see `oceanoptics_spectrometer/github_example.py`. For a simple client, see `oceanoptics_spectrometer/clients/client.py`.

For a detailed example, see `oceanoptics_spectrometer/device.py`. To run it, execute script `oceanoptics_spectrometer/executor.py`

requirements:
- your hardware
- [hololinked](https://github.com/VigneshVSV/hololinked) (`pip install hololinked`)
- seabreeze (`pip install seabreeze`)
- for HTTPs - you need a key and certificate otherwise intensity events will not work on the web browser. for HTTP (i.e. not HTTPs), you then need to poll the intensity on your own. 

Contributors welcome for any features, tests and examples on models that I do not possess, raise issue/discussion or drop me an [email](vignesh.vaidyanathan@hololinked.dev). This is only an example, its meant to be modified according to your requirements. If the requirements are very common case, it can be included directly here. Few parts also require optimization. Fork and create PR, dont hesitate to contact me. Despite irregular commit history, repository will be actively maintained. 

### Supported

Properties
- serial number
- integration time
- trigger mode
- last measured intensity
- reference intensity 
- model
- wavelengths
- pixel count
- max intensity
- background correction
- custom background intensity (untested)
- non linearity correction

Actions
- connect/disconnect 
- start/stop measurement
- single measurement (untested)

Events
- intensity measurement event
- state change event
- trigger mode and integration time change event

### GUI

browser GUIs are available: 

- node-wot (Web Of Things) based Svelte smartphone app
    - compatible with smartphone and smaller screens, FPS based on smartphone power.
    - follow instructions [here](https://gitlab.com/node-clients/spectrometer-smartphone)
    - refactor components for larger monitors, 30 FPS or higher possible in general on PCs. 
- panel GUI `oceanoptics_spectrometer/clients/holoviz_panel.ipynb`
- PyQt? you can send me an implementation if you have

### Future

- cookie based authentication will be added